package agents_uds;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import madkit.kernel.AbstractAgent;
import madkit.kernel.Agent;
import madkit.message.StringMessage;
/**
 * 
 * @author Kana Patrick
 *
 */
public class Bus extends Agent {
	
	Date t ;
	public	List<AbstractAgent> etuaccess ;
	int teming;
 
	public void activate(){
		teming = 2000;
		t = new Date();
		etuaccess = new ArrayList<AbstractAgent>();
		createGroupIfAbsent("Poste navette","campus");
		requestRole("Poste navette","campus","bus",null);
	}
	

	@SuppressWarnings("deprecation")
	public void live(){
		pause(5000);
		
		while(true){
		
			Etudiant e = new Etudiant();
			StringMessage m = (StringMessage) waitNextMessage(10000);
			
			if(m != null){
				
				if(m.getContent().equals("access ok")){
					
					etuaccess.add(e);
					
					if(logger != null){
					 
						logger.info("\n D�ja \t"+etuaccess.size()+"\t dans le Bus � \t"+t.getHours()+"H"+t.getMinutes()+"mn"+t.getSeconds()+"s");
					}
				}
			}
			
			if(etuaccess.size() == 30){
				
				if(logger != null ){
					
					logger.info("je pleinn et je decolle");
					killAgent(this);
					
				}
				
				for (int i =0 ; i < etuaccess.size();i++){
					
					AbstractAgent ad = etuaccess.remove(i);
					if(logger != null){
						logger.info("la mort de l 'etudiant"+i);
						killAgent(ad);
					}
					
				}
			}
			
			if(etuaccess.isEmpty()){
				
				killAgent(this);
			}
			
			
			
		}
	}
	
}
