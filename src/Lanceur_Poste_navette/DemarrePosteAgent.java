package Lanceur_Poste_navette;

import java.util.ArrayList;

import agents_uds.Bus;
import agents_uds.Caissier;
import agents_uds.Chargeur;
import agents_uds.Etudiant;
import madkit.kernel.AbstractAgent;
import madkit.kernel.Agent;
import madkit.kernel.Madkit;
/**
 * 
 * @author Kana Patrick
 *
 */

public class DemarrePosteAgent extends Agent {
	
	ArrayList<AbstractAgent> listetudiant ;
 	public void activate(){
 		int initpause = 2000;
		
 		listetudiant = new ArrayList<AbstractAgent>();
 		
		for(int i =0 ; i<4 ; i++){
			Etudiant etu = new Etudiant();
			if(launchAgent(etu,true) == ReturnCode.SUCCESS){
				
				listetudiant.add(etu);
				if(logger != null){
					
					logger.info("Etudiant numero \t"+i+"\t � la caisse");
					
				}
			
				pause((initpause > 0 ? initpause : 20));
				initpause -= Math.random() * 100;
				
			}
		}
		
		Caissier cai = new Caissier();
		if(launchAgent(cai,true) == ReturnCode.SUCCESS){
			listetudiant.add(cai);
			if(logger != null ){
				logger.info("le Caissier DEMarre");
				logger.info("je suis � la caisse et pr�s a encaisser "+ cai.somjour);
				
			}
			pause((initpause > 0 ? initpause : 20));
			initpause -= Math.random() * 100;
		}
		
		for(int i =0 ; i < 2 ; i++){
			
			Bus b = new Bus();
			if(launchAgent(b,true) == ReturnCode.SUCCESS){
				listetudiant.add(b);
				
				if(logger != null ){
					logger.info("le Bus Launched");
				}
			pause(initpause > 0 ? initpause : 20);
			initpause -= Math.random()*100;
			}
		}
		
		Chargeur c = new Chargeur();
		
		if(launchAgent(c,true) == ReturnCode.SUCCESS){
			
			listetudiant.add(c);
			if(logger != null){
				logger.info("Chargeur Launched");
			}
			
			pause(initpause > 0 ? initpause : 20);
			
			initpause -= Math.random() *100;
		}
		
	}
	
	public  void live(){
		pause(Integer.MAX_VALUE);
	}
	
	@Override
	public  void end(){
		
		int  temps = 3000;
		
		while(!listetudiant.isEmpty()){
			
			AbstractAgent ag = listetudiant.remove((int) Math.random() * 100);
			killAgent(ag);
			pause(temps > 0 ? temps : 20);
			
			temps -= Math.random() * 100;
			
			if(logger != null){
				
				logger.info("plus aucun "+ listetudiant);
			}
		}
		
	}

	public static void main(String[] args){
		
		String [] argss = {"--AgentLogLevel","INFO","--LaunchAgent",DemarrePosteAgent.class.getName()};
		
		Madkit.main(argss);
	}
}
