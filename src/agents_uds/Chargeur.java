package agents_uds;

import java.util.ArrayList;

import madkit.kernel.AbstractAgent;
import madkit.message.StringMessage;

/**
 * 
 * @author Kana Patrick
 *
 */
public class Chargeur extends Personnage {
	
	ArrayList<AbstractAgent> listagentdefile;
	int compteticket=0;
	
public void activate(){
	listagentdefile = new ArrayList<AbstractAgent>();
	createGroupIfAbsent("Poste navette","campus");
	requestRole("Poste navette","campus","chargeur",null);
}

public void live(){
	pause(5000);
	
	while(true){
		StringMessage m = (StringMessage) waitNextMessage(2000);
		
		if( m!=null){
			
			if(m.getContent().equals("coupe ticket"))
			{
				if(Math.random() > 0.75){
					if(logger !=null){
						logger.info("le ticket de \t"+m.getSender()+" est coup�" );
						compteticket++;
					}					
					
				}	
			}
			if(Math.random() < 0.70){
				sendMessageWithRole("Poste navette","campus","bus", new StringMessage("access ok"),"chargeur");
			}
	}
}
}
}
