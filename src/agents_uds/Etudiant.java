package agents_uds;

import madkit.message.StringMessage;

/**
 * 
 * @author Kana Patrick
 *
 */
public class Etudiant extends Personnage {

	boolean dispo = false; // permet de verifier l 'etat de disponibilité de l agent qu'on veux joindre;
	
	
	public void activate(){
		
		createGroupIfAbsent("Poste navette","campus");
		requestRole("Poste navette","campus","etudiant",null);
	}
	
	
	public void live(){
		
		pause(5000);
		
		while(true){
			
			StringMessage m = (StringMessage) waitNextMessage(1000);
			
			if(m != null ){
				
				if(m.getContent().equals("indisponible")){
					
					   if(logger != null){
						   
						   logger.info("Aucune personne au  poste de navette");
						   killAgent(this);  
					   }
						   
					 }else{
						  
						  if(logger != null){
							  
							   logger.info("j'achete mon ticket de navette");
							   
			                }
						  
					  }
						
			    }
			
			   if(Math.random() < 0.75) {
					  
				    sendMessageWithRole("Poste navette","campus", "caissier", new StringMessage("je veux un ticket??"),"etudiant");
				    
				    sendMessageWithRole("Poste navette","campus", "caissier", new StringMessage("Caissier Absent??"),"etudiant");
				    sendMessageWithRole("Poste navette","campus","chargeur", new StringMessage("coupe ticket"),"etudiant");
				    
				 }				
		}		
	}
}

