package agents_uds;

import madkit.message.StringMessage;

/**
 * 
 * @author Kana Patrick
 *
 */

public class Caissier extends Personnage {

	public int somjour = 0;
	public int nombreErreur;
	public void activate(){
		
		createGroupIfAbsent("Poste navette","campus");
		requestRole("Poste navette","campus","caissier",null);
	
	}
	
	public void live(){
		pause(5000);
		while(true){
			
		
			
			if( this.nombreErreur == 2){
				
				if(logger != null ){
					logger.info("j ai pas encore eu d'etudiant pour achat de ticket");
				}
			}else{ 
				
					if(Math.random() > 0.95){
						
						
						sendMessageWithRole("Poste navette","campus","etudiant", new StringMessage("tient ton ticket"),"caissier");
					}
			}
			
			StringMessage m = (StringMessage) waitNextMessage(2000);
			
			if(m != null){
				
				if(m.getContent().equals("je veux un ticket??")){
					
					sendMessage(m.getSender(), new StringMessage("tient le ticket"));
					waitNextMessage(1000);
					somjour += 50;
					if(logger != null){
						
						logger.info("Mon montant en Caisse est de :\t"+somjour+"Fcfa");
						
						logger.info("Alignez vous Mr / Mme");
					}
				}
				
				if(m.getContent().equals("Caissier Absent??")){
					
					nombreErreur++;
					
					sendMessage(m.getSender(),new StringMessage("indisponible"));
				}
			}

		
		}
	}

}
